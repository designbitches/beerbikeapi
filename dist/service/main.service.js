"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const beerbikeLog_model_1 = require("../models/beerbikeLog.model");
const beerbikeSpeed_model_1 = require("../models/beerbikeSpeed.model");
const beerbikeSettings_model_1 = require("../models/beerbikeSettings.model");
class MainService {
    getSettings(req, res) {
        beerbikeSettings_model_1.BeerBikeSettingModel.findOne({ _id: '5df231a299691703e4ea88f8' }, (err, doc) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(doc);
        });
    }
    updateSettings(req, res) {
        if (!req.body) {
            return res.status(400).json({ msg: 'invalid body' });
        }
        beerbikeSettings_model_1.BeerBikeSettingModel.updateOne({ _id: '5df231a299691703e4ea88f8' }, req.body, { new: true }, (err, doc) => {
            console.log(err);
            if (doc) {
                beerbikeSettings_model_1.BeerBikeSettingModel.findOne({ _id: '5df231a299691703e4ea88f8' }, (err, doc) => {
                    if (err) {
                        return res.status(400).json(err);
                    }
                    res.status(200).json(doc);
                });
            }
        });
    }
    getSpeed(req, res) {
        beerbikeSpeed_model_1.BeeBikeSpeedModel.findOne({}, (err, data) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(data);
        }).sort({ timeStamp: -1 });
    }
    setSpeed(req, res) {
        if (!req.body.speed) {
            return res.status(400).json({ msg: 'invalid body' });
        }
        const now = new Date();
        const speed = {
            speed: req.body.speed,
            time: now,
            timeStamp: now.getTime()
        };
        beerbikeSpeed_model_1.BeeBikeSpeedModel.create(speed).then(data => {
            res.status(200).json(data);
        })
            .catch(error => {
            return res.status(400).json(error);
        });
    }
    setCommand(req, res) {
        if (!req.params.team) {
            return res.status(400).json({ msg: 'no team found' });
        }
        if (!req.params.command) {
            return res.status(400).json({ msg: 'no command param found' });
        }
        if (!req.body.rounds) {
            req.body.rounds = 0;
        }
        const team = req.params.team;
        const command = req.params.command;
        if (team != 'blue' && team != 'red') {
            return res.status(400).json({ msg: 'invalid team name' });
        }
        if (command != 'start' && command != 'stop') {
            return res.status(400).json({ msg: 'invalid command name' });
        }
        const now = new Date();
        const log = {
            team: req.params.team,
            command,
            time: now,
            timeStamp: now.getTime(),
            rounds: req.body.rounds
        };
        beerbikeLog_model_1.BeerBikeLogModel.create(log)
            .then(data => {
            res.status(200).json(data);
        })
            .catch(error => {
            return res.status(400).json(error);
        });
    }
    getStatus(req, res) {
        beerbikeLog_model_1.BeerBikeLogModel.findOne({}, (err, data) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(data);
        }).sort({ timeStamp: -1 });
    }
    getResult(req, res) {
        beerbikeLog_model_1.BeerBikeLogModel.find({}, (err, logs) => {
            if (err) {
                return res.status(400).json(err);
            }
            let roundsTeamBlue = 0;
            let roundsTeamRed = 0;
            if (logs) {
                logs.forEach((log) => {
                    if (log.team == 'blue') {
                        if (log.rounds) {
                            roundsTeamBlue += log.rounds;
                        }
                    }
                    if (log.team == 'red') {
                        if (log.rounds) {
                            roundsTeamRed += log.rounds;
                        }
                    }
                });
            }
            return res.status(200).json({ roundsTeamRed: roundsTeamRed, roundsTeamBlue: roundsTeamBlue });
        });
    }
}
exports.MainService = MainService;
