"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const BeerBikeLogSchema = new mongoose_1.Schema({
    team: String,
    command: String,
    rounds: Number,
    time: Date,
    timeStamp: Number
});
exports.BeerBikeLogModel = mongoose_1.model('BeerBikeLog', BeerBikeLogSchema);
