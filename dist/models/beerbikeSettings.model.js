"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const BeerBikeSettingsSchema = new mongoose_1.Schema({
    mode: String,
    maxKm: Number
});
exports.BeerBikeSettingModel = mongoose_1.model('BeerBikeSetting', BeerBikeSettingsSchema);
