"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const BeeBikeSpeedSchema = new mongoose_1.Schema({
    speed: Number,
    time: Date,
    timeStamp: Number
});
exports.BeeBikeSpeedModel = mongoose_1.model('BeerBikeSpeed', BeeBikeSpeedSchema);
