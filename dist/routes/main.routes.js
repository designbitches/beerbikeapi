"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const main_service_1 = require("../service/main.service");
const router = express_1.default.Router();
const mainService = new main_service_1.MainService();
router.route('/').get(function (req, res, next) {
    res.status(200).json({ msg: 'hello from server' });
});
router.route('/result').get(mainService.getResult);
router.route('/status').get(mainService.getStatus);
router.route('/:team/:command').post(mainService.setCommand);
router.route('/status').post(mainService.setSpeed);
router.route('/settings').put(mainService.updateSettings);
router.route('/settings').get(mainService.getSettings);
router.route('/speed').get(mainService.getSpeed);
module.exports = router;
