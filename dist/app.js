"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const mongoose_1 = __importDefault(require("mongoose"));
const app = express_1.default();
const config = require('./config');
mongoose_1.default.connect('mongodb://' + config.db.host + '/' + config.db.name, config.db.options);
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorisation, Authorization, Accept, X-Requested-With, cache-control');
    res.setHeader('Content-Type', 'application/json');
    next();
});
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use(body_parser_1.default.json({ limit: '50mb' }));
app.use('/', require('./routes/main.routes'));
app.listen(config.app.port, () => {
    console.log("server runs on " + config.app.port + " in Version ");
});
