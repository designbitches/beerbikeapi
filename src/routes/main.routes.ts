import express, {Application, Request, Response, NextFunction} from 'express';
import {MainService} from '../service/main.service';

const router = express.Router();
const mainService = new MainService();

router.route('/').get(
    function (req: any, res: any, next: any) {
        res.status(200).json({msg: 'hello from server'})
    });


router.route('/result').get(mainService.getResult);
router.route('/status').get(mainService.getStatus);
router.route('/:team/:command').post(mainService.setCommand);
router.route('/status').post(mainService.setSpeed);
router.route('/settings').put(mainService.updateSettings);
router.route('/settings').get(mainService.getSettings);
router.route('/speed').get(mainService.getSpeed);


module.exports = router;
