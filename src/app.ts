import express, {Application, Request, Response, NextFunction} from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
const app = express();
const config = require('./config');


mongoose.connect('mongodb://' + config.db.host + '/' + config.db.name, config.db.options );


app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorisation, Authorization, Accept, X-Requested-With, cache-control');
    res.setHeader('Content-Type', 'application/json');
    next();
});


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));


app.use('/', require('./routes/main.routes'));

app.listen(config.app.port, () => {
    console.log("server runs on " + config.app.port + " in Version ");
});
