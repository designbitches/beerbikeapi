import {BeerBikeLogInterface, BeerBikeLogModel} from '../models/beerbikeLog.model';
import {BeeBikeSpeedModel, BeerbikeSpeedInterface} from '../models/beerbikeSpeed.model';
import {BeerBikeSettingModel, BeerBikeSettingsInterface} from '../models/beerbikeSettings.model';

export class MainService {

    getSettings(req: any, res: any) {


        BeerBikeSettingModel.findOne({_id: '5df231a299691703e4ea88f8'}, (err: any, doc: any) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(doc);
        })
    }

    updateSettings(req: any, res: any) {
        if (!req.body) {
            return res.status(400).json({msg: 'invalid body'});
        }

        BeerBikeSettingModel.updateOne({_id: '5df231a299691703e4ea88f8'}, req.body, {new: true }, (err: any, doc: any) => {
                console.log(err);
                if (doc) {
                    BeerBikeSettingModel.findOne({_id: '5df231a299691703e4ea88f8'}, (err: any, doc: any) => {
                        if (err) {
                            return res.status(400).json(err);
                        }
                        res.status(200).json(doc);
                    })
                }
        })
    }

    getSpeed(req: any, res: any) {

        BeeBikeSpeedModel.findOne({}, (err: any, data: any) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(data);
        }).sort({timeStamp: -1})
    }

    setSpeed(req: any, res: any) {

        if (!req.body.speed) {
            return res.status(400).json({msg: 'invalid body'});
        }

        const now = new Date();
        const speed: BeerbikeSpeedInterface = {
            speed: req.body.speed,
            time: now,
            timeStamp: now.getTime()

        }
        BeeBikeSpeedModel.create(speed).then(data => {
            res.status(200).json(data);
        })
            .catch(error => {
                return res.status(400).json(error);
            })
    }

    setCommand(req: any, res: any) {

        console.log('triggerd');

       if (!req.params.team) {
           return res.status(400).json({msg: 'no team found'});
       }
        if (!req.params.command) {
            return res.status(400).json({msg: 'no command param found'});
        }

        if (!req.body.rounds) {
            req.body.rounds = 0;
        }

        const team = req.params.team;
        const command = req.params.command;
       if (team != 'blue' && team != 'red') {
           return res.status(400).json({msg: 'invalid team name'});
       }

        if (command != 'start' && command != 'stop') {
            return res.status(400).json({msg: 'invalid command name'});
        }

       const now = new Date();
       const log: BeerBikeLogInterface = {
           team: req.params.team,
           command,
           time: now,
           timeStamp: now.getTime(),
           rounds: req.body.rounds

       }



       BeerBikeLogModel.create(log)
           .then(data => {
               res.status(200).json(data);
           })
           .catch(error => {
               return res.status(400).json(error);
           })

    }

    getStatus(req: any, res: any) {
        BeerBikeLogModel.findOne({}, (err: any, data: any) => {
            if (err) {
                return res.status(400).json(err);
            }
            res.status(200).json(data);
        }).sort({timeStamp: -1})
    }

    getResult(req: any, res: any) {
        BeerBikeLogModel.find({}, (err: any, logs: any) => {
            if (err) {
                return res.status(400).json(err);
            }
            let roundsTeamBlue = 0;
            let roundsTeamRed = 0;
            if (logs) {
                logs.forEach((log: BeerBikeLogInterface) => {
                    if (log.team == 'blue') {
                        if (log.rounds) {
                            roundsTeamBlue += log.rounds;
                        }

                    }
                    if (log.team == 'red') {
                        if (log.rounds) {
                            roundsTeamRed += log.rounds;
                        }
                    }
                });
            }

            return res.status(200).json({roundsTeamRed: roundsTeamRed, roundsTeamBlue: roundsTeamBlue});
        })
    }


}
