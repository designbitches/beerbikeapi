import {Schema, Model, model, Document} from 'mongoose';



export interface BeerBikeLogInterface {
    _id?: string;
    team: string; // red, blue
    command: string;  // start, stop
    rounds: number; // number of rounds
    time: Date;
    timeStamp: number;

}

const BeerBikeLogSchema: Schema = new Schema<any>({
    team: String, // red, blue
    command: String, // start, stop
    rounds: Number, // number of rounds
    time: Date,
    timeStamp: Number


})


export const BeerBikeLogModel: Model<any> = model<any>('BeerBikeLog', BeerBikeLogSchema);



