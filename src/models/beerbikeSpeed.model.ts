import {Schema, Model, model, Document} from 'mongoose';



export interface BeerbikeSpeedInterface {
    _id?: string;
    speed: number;
    time: Date;
    timeStamp: number;

}

const BeeBikeSpeedSchema: Schema = new Schema<any>({
    speed: Number,
    time: Date,
    timeStamp: Number


})


export const BeeBikeSpeedModel: Model<any> = model<any>('BeerBikeSpeed', BeeBikeSpeedSchema);



