import {Schema, Model, model, Document} from 'mongoose';



export interface BeerBikeSettingsInterface {
    _id?: string;
    mode: string;
    maxKm: number;

}

const BeerBikeSettingsSchema: Schema = new Schema<any>({
    mode: String, // child / adult
    maxKm: Number


})


export const BeerBikeSettingModel: Model<any> = model<any>('BeerBikeSetting', BeerBikeSettingsSchema);



