module.exports = {
    "app": {
        "port": 8877
    },
    "db": {
        "host": "localhost",
        "port": 27017,
        "name": "beerBike",
        "options": {
            "useNewUrlParser": true,
            "useFindAndModify": false,
            "useUnifiedTopology": true
        }
    },
};
